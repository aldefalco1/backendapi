#!/bin/sh

echo "Waiting for database..."

#TODO: configure database host and port
while ! nc -z database 5432; do
  sleep 0.5
done

echo "Database started"

#TODO: configure workers number
gunicorn -w 8 -b 0.0.0.0:5000 manage:app
