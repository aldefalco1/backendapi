from flask.cli import FlaskGroup
from restaurant import create_app, db
from restaurant.menu import models
import unittest

cli = FlaskGroup(create_app=create_app)
app = create_app()


@cli.command()
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def seed_db():
    r1 = models.MenuItem(id='R0001', restaurant_id='RS01', title='Salad', description='')
    r2 = models.MenuItem(id='R0002', restaurant_id='RS01', title='Pizza', description='')
    r3 = models.MenuItem(id='R0003', restaurant_id='RS02', title='Hamburger', description='')
    r1.modifiers = [
        models.Modifier(id='M01', article=models.Ingredient(id='A01', title='Topping', description='')),
        models.Modifier(id='M02', article=models.Ingredient(id='A02', title='Sauce #1', description=''), modifiers=[
            models.Modifier(id='M04', article=models.Ingredient(id='A03', title='Sauce #2', description='')),
            models.Modifier(id='M05', article=models.Ingredient(id='A05', title='Onion', description=''))
        ])
    ]
    db.session.add(r1)
    db.session.add(r2)
    db.session.add(r3)
    db.session.commit()


@cli.command()
def test():
    tests = unittest.TestLoader().discover('restaurant/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


if __name__ == '__main__':
    cli()