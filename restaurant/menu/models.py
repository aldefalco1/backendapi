from restaurant import db


class MenuItem(db.Model):
    id = db.Column(db.String(32), primary_key=True)
    restaurant_id = db.Column(db.String(32))
    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(150), nullable=True)
    modifiers = db.relationship('Modifier', back_populates='item')

    def __repr__(self):
        return '<Item %r %r>' % (self.id, self.title)


class Modifier(db.Model):
    id = db.Column(db.String(32), primary_key=True)
    parent_id = db.Column(db.String(32), db.ForeignKey("modifier.id"), nullable=True)
    item_id = db.Column(db.String(32), db.ForeignKey("menu_item.id"), nullable=True)
    ingredient_id = db.Column(db.String(32), db.ForeignKey("ingredient.id"), nullable=True)
    item = db.relationship('MenuItem', back_populates='modifiers')
    ingredient = db.relationship('Ingredient')
    modifiers = db.relationship('Modifier')

    def __repr__(self):
        return '<Modifier %r %r>' % (self.id, self.item_id)


class Ingredient(db.Model):
    id = db.Column(db.String(32), primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(150), nullable=True)

    def __repr__(self):
        return '<Ingredient %r %r>' % (self.id, self.title)
