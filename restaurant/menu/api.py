from flask_restplus import Resource, Namespace, fields
from sqlalchemy.orm import joinedload, contains_alias, contains_eager
from restaurant import db
from sqlalchemy import null

from .models import MenuItem, Modifier
import logging

logger = logging.getLogger(__name__)

api = Namespace('Menu', description='Menu items operations', path='/restaurants')

ingredient_model = api.model('Ingredient', {
    'id': fields.String(description='Ingredient unique id', example='A-001'),
    'title': fields.String(description='Ingredient title', example='Sauce'),
    'description': fields.String(description='Menu title', example='Sauce cream'),
}, description='Ingredient')

modifier_model = api.model('Modifier', {
    'id': fields.String(description='Modifier unique id', example='M-001'),
    'ingredient': fields.Nested(ingredient_model, description='Ingredient'),
})

complex_modifier_model = api.model('ComplexModifier', {
    'id': fields.String(description='Modifier unique id', example='M-001'),
    'ingredient': fields.Nested(ingredient_model, description='Ingredient'),
    'modifiers': fields.List(fields.Nested(modifier_model), description='Secondary modifiers')
})

# TODO: this is example how to make recursive hierarchy of modifiers
"""
def recursive_modifier_model_mapping(iteration_number=3):
    json_mapping = {
        'id': fields.String,
        'article': fields.Nested(article_model),
    }
    if iteration_number:
        json_mapping['modifiers'] = fields.List(fields.Nested(recursive_modifier_model_mapping(iteration_number - 1)))
    return api.model('Modifier' + str(iteration_number), json_mapping)


modifier_model['modifiers'] = fields.List(fields.Nested(modifier_model))
"""

item_model = api.model('Item', {
    'id': fields.String(description='The menu item unique identifier', example='I000-0001'),
    'title': fields.String(description='Menu title', example='Pizza Margarita'),
    'description': fields.String(description='Menu description', example='Health food'),
    'modifiers': fields.List(fields.Nested(complex_modifier_model), description='Modifiers of menu item', example=[])
})


@api.route('/ping')
@api.doc(description='Ping')
class Ping(Resource):
    """Ping healthy checker"""

    def get(self):
        return 'Pong'


# TODO: According to the RESTful specification it should be named in plural form as 'items'
@api.route('/<string:restaurant_id>/item')
@api.doc(description='Menu item resources access points')
class MenuApi(Resource):
    """Menu item resource controller"""

    @api.marshal_list_with(item_model)
    @api.doc(
        description='Get all menu items for selected restaurant',
        responses={
            200: 'Success',
            500: 'Server error'
        }, params={'restaurant_id': 'Restaurant unique identifier'})
    def get(self, restaurant_id):
        """
        Get all menu items for selected restaurant
        :param restaurant_id: The restaurant id
        """
        logger.debug('Menu item call for "%s"', restaurant_id)
        try:
            # TODO: There are some performance issues. First, there is a lazy loading and select N and we need to use
            # the eager loading instead(resolved). Second is the hierarchy of modifiers,
            # and we need to use recursive
            # CTE (it's quite enough in this case) or some tree indexes

            # top = db.session.query(Modifier)\
            #     .filter(Modifier.parent_id == null())\
            #     .cte(name='m', recursive=True)
            #
            # parents = db.aliased(top)
            # children = db.aliased(Modifier)
            #
            # hierarchy = top.union_all(db.session.query(children)
            #                           .join(parents, children.id == parents.c.parent_id)).alias('h')
            #
            # return MenuItem.query. \
            #     outerjoin((hierarchy, MenuItem.modifiers)) .\
            #     options(contains_eager(MenuItem.modifiers, alias=hierarchy)) \
            #     .filter(MenuItem.restaurant_id == restaurant_id).all()

            return MenuItem.query.options(
                    joinedload(MenuItem.modifiers).
                    joinedload(Modifier.ingredient)) \
                .filter(MenuItem.restaurant_id == restaurant_id).all()
        except Exception as e:
            logger.exception('Error: %s', e)
            raise
