import  os
from flask import Flask
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from .menu.api import api as recipe_ns

api = Api(
    version='1.0',
    title='Restaurants menu API',
    description='The restaurant menu resources API provides assess to data'
)

api.add_namespace(recipe_ns)


def create_app(config=None):
    config = config or os.environ.get('FLASK_CONFIG', 'restaurant.config.DevelopmentConfig')
    app = Flask(__name__)
    app.config.from_object(config)
    api.init_app(app)
    db.init_app(app)
    return app
