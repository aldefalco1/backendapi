from flask_testing import TestCase

from restaurant import create_app, db
from restaurant.menu.models import MenuItem, Modifier, Ingredient


class MenuTest(TestCase):

    def create_app(self):
        app = create_app('restaurant.config.TestingConfig')
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_all_items_with_complex_modifiers(self):
        a1 = Ingredient(id='A01', title='Salad')
        a2 = Ingredient(id='A02', title='Salad2')
        r1 = MenuItem(id='R001', title='Test', description='bbb', restaurant_id='000')
        r2 = MenuItem(id='R002', title='Test 2', description='aa aa', restaurant_id='000')
        r2.modifiers = [
            Modifier(id='M01', ingredient=a1),
            Modifier(id='M02', ingredient=a2, modifiers=[
                Modifier(id='M03', ingredient=a1),
                Modifier(id='M04', ingredient=a1),
            ])
        ]
        db.session.add(r1)
        db.session.add(r2)
        db.session.commit()

        result = self.client.get('/restaurants/000/item')

        self.assert200(result, 'OK')
        self.assertEqual(2, len(result.json), 'Only two restaurants should be returned')
        self.assertEqual('R001', result.json[0]['id'], 'First menu item is not correct')
        self.assertEqual('R002', result.json[1]['id'], 'Second menu item is not correct')
        self.assertEqual(2, len(result.json[1]['modifiers']), 'Primary modifiers are not correct')
        self.assertEqual(2, len(result.json[1]['modifiers'][1]['modifiers']), 'Secondary modifiers are not correct')

    def test_get_all_items_for_selected_restaurant_only(self):
        r1 = MenuItem(id='R001', title='Test 2', description='', restaurant_id='R-0001')
        r2 = MenuItem(id='R002', title='Test 2', description='', restaurant_id='R-0001')
        r3 = MenuItem(id='R003', title='Test 2', description='', restaurant_id='R-0002')
        db.session.add(r1)
        db.session.add(r2)
        db.session.add(r3)
        db.session.commit()

        result = self.client.get('/restaurants/R-0001/item')

        self.assert200(result, 'OK')
        self.assertEqual(2, len(result.json), 'Only two restaurants should be returned')
